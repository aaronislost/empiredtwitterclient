# NOTES #

### Assumptions ###

I have made a few assumptions while building this

* the twitter api will never change
* whoever is looking at this has visual studio/git installed
* no one will abuse the twitter consumer_key/secret checked into a public repo

### How do I get set up? ###

* git clone https://bitbucket.org/aaronislost/empiredtwitterclient.git
* open the TwitterClient.sln in visual studio
* press f5

### What has not been implemented? ###

* Any security.
* Any validation - there isnt any attempt to see if twitter is currently up or a retry strategy.
* only the q paramater has been implemented.
* geocode, lang, locale, result_type, count, until, since_id, max_id, include_entities still need to be done.
* an installer

### What I have learned? ###

* things depending HttpWebRequest require quite a bit of thought on how to make them testable.