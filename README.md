# README #

This is a twitter to returns a collection of relevant Tweets matching a specified query.

### What is this repository for? ###

Twitter exposes a REST API that allows clients to perform a wide variety of queries against Twitter's database, this is a REST client to search the twitter API documented at  https://dev.twitter.com/rest/reference/get/search/tweets

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contact info ###

* email  : aaron.herbert@gmail.com
* phone  : 0401 866 690
* resume : http://aaronislost.com/ResumeOfAaronHerbert.pdf