﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterClient.Common;

namespace TwitterClient.Business
{
    public interface ITwitterService 
    {
        List<SearchResponse> Search(SearchModel model);
        void SetAuthToken();

    }
}
