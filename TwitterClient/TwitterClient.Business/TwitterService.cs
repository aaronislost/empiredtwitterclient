﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TwitterClient.Common;

namespace TwitterClient.Business
{

    public interface IConfiguration
    {
        string consumer_secret { get; }
        string consumer_key { get; }
    }

    public class Configuration : IConfiguration  
    {
        public string consumer_secret { get { return ConfigurationManager.AppSettings["consumer_secret"]; } }
        public string consumer_key { get { return ConfigurationManager.AppSettings["consumer_key"]; } }
    }



    public class TwitterService : ITwitterService
    {
        public Token _authToken { get; set; }
        private IHttpService _http;
        private IConfiguration _config;

        public TwitterService(IHttpService http, IConfiguration config)
        {
            this._http = http;
            this._config = config;
        }

        public void SetAuthToken()
        {
            var oauth_consumer_key = _config.consumer_key;
            var oauth_consumer_secret = _config.consumer_secret;

            //Token URL
            var oauth_url = "https://api.twitter.com/oauth2/token";
            var headerFormat = "Basic {0}";
            var authHeader = string.Format(headerFormat,
            Convert.ToBase64String(Encoding.UTF8.GetBytes(Uri.EscapeDataString(oauth_consumer_key) + ":" +
            Uri.EscapeDataString((oauth_consumer_secret)))
            ));

            var postBody = "grant_type=client_credentials";

            ServicePointManager.Expect100Continue = false;
            HttpWebRequest request = _http.CreateRequest(oauth_url);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";

            using (Stream stream = request.GetRequestStream())
            {
                byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                stream.Write(content, 0, content.Length);
            }

            this._authToken = this._http.DoRequest<Token>(request);
        }

        public List<SearchResponse> Search(SearchModel model)
        {
            HttpWebRequest request = _http.CreateRequest("https://api.twitter.com/1.1/search/tweets.json?q=" + model.Query);

            request.Credentials = CredentialCache.DefaultCredentials;
            request.Headers.Add("Authorization", this._authToken.token_type + " " + this._authToken.access_token);
            request.Method = "GET";

            var response = this._http.DoRequest<TwitterResponse>(request);

            return response.statuses.Select(x => new SearchResponse(x)).ToList();


        }

    }
}
