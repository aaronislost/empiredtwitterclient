﻿using Newtonsoft.Json;
using System.IO;
using System.IO.Compression;
using System.Net;
using TwitterClient.Common;

namespace TwitterClient.Business
{

    public class HttpService : IHttpService
    {
        public HttpService()
        {

        }

        public T DoRequest<T>(HttpWebRequest request)
        {
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            Stream responseStream = new GZipStream(response.GetResponseStream(), CompressionMode.Decompress);
            var serializer = new JsonSerializer();

            using (var reader = new StreamReader(responseStream))
            {
                using (var jsonTextReader = new JsonTextReader(reader))
                {
                    return serializer.Deserialize<T>(jsonTextReader);

                }
            }
        }

        public HttpWebRequest CreateRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers.Add("Accept-Encoding", "gzip");

            return request;
        }
    }
}
