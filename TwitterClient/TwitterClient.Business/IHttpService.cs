﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TwitterClient.Business
{
    public interface IHttpService
    {
        T DoRequest<T>(HttpWebRequest request);

        HttpWebRequest CreateRequest(string url);
    }
}
