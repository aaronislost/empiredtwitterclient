﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TwitterClient.Business;
using TwitterClient.Common;

namespace TwitterClient
{

//    async
//form
//linq to twitter
//bind to ui
//IOC
//logging
//tests
//results
    public partial class frmMain : Form
    {
       private  Token _authToken { get; set; }
        private ITwitterService _twitterService { get; set; }
        public frmMain(ITwitterService twitterService)
        {
            this._twitterService = twitterService;
            InitializeComponent();

           
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchModel model = new SearchModel()
            {
                Query = txtSearch.Text
            };

            
            var x = _twitterService.Search(model);
            this.dgResult.DataSource = x;
            
        }

        private async void frmMain_Load(object sender, EventArgs e)
        {
            await Task.Run(() => this._twitterService.SetAuthToken());
            
            //TODO Some kind of loading / authenticating screen
            this.btnSearch.Enabled = true;
        }
   
    }
}


 