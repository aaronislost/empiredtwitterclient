﻿using Autofac;
using System;
using System.Windows.Forms;
using TwitterClient.Business;

namespace TwitterClient
{
    static class Program
    {
        private static IContainer Container { get; set; }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<HttpService>().As<IHttpService>();
            builder.RegisterType<TwitterService>().As<ITwitterService>();
            builder.RegisterType<TwitterClient.Business.Configuration>().As<IConfiguration>();

            Container = builder.Build();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain(Container.Resolve<ITwitterService>()));

        }
    }
}
