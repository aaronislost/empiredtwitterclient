﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwitterClient.Common
{
    public class SearchResponse
    {
        public string User { get; set; }
        public string Text { get; set; }

        public SearchResponse() { }

        public SearchResponse(Status status)
        {
            this.Text = status.text;

            var name = status?.user?.name;

            if (name == null)
            {
                name = "Unknown";
            } else
            {
                name = "@" + name;
            }
            this.User = name;
        }
    }
}
