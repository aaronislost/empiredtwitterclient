﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwitterClient.Business;
using Moq;
using Shouldly;
using System.Diagnostics;
using Autofac;
using TwitterClient.Common;
using System.Net;
using System.Dynamic;
using System.Collections.Generic;

namespace TwitterClient.Test
{
    [TestClass]
    public class TwitterServiceTests : TestBase
    {
        private IHttpService _http { get; set; }

        [TestInitialize]
        public void init()
        {
            
        }
        
        [TestMethod]
        public void TwitterService_should_user_model_to_hit_correct_endpoint()
        {
            var configMock = this.GetConfigMock();
            var httpMock = this.GetHttpMock();



            TwitterService service = new TwitterService(httpMock.Object, configMock.Object);
            service._authToken = new Token() { access_token = "asd", token_type = "qwe" };
            service.Search(new SearchModel() { Query = "test" });

            httpMock.Verify(x => x.CreateRequest("https://api.twitter.com/1.1/search/tweets.json?q=test"));

        }

        [TestMethod]
        public void TwitterService_should_set_the_auth_token()
        {
            var configMock = this.GetConfigMock();
            var httpMock = this.GetHttpMock();


           
            httpMock.Setup(c => c.DoRequest<Token>(It.IsAny<HttpWebRequest>()))
                .Returns(new Token() { access_token = "token", token_type = "type" });

            TwitterService service = new TwitterService(httpMock.Object, configMock.Object);
 
            service.SetAuthToken();

            service._authToken.access_token.ShouldBe("token");
            service._authToken.token_type.ShouldBe("type");
            
        }

        private Mock<IConfiguration> GetConfigMock()
        {
            var configMock = new Mock<IConfiguration>();
            configMock.SetupGet(x => x.consumer_key).Returns("");
            configMock.SetupGet(x => x.consumer_secret).Returns("");

            return configMock;
        }

        private Mock<IHttpService> GetHttpMock()
        {
            var httpMock = new Mock<IHttpService>();
            httpMock.Setup(c => c.CreateRequest(It.IsAny<string>()))
                .Returns((HttpWebRequest)WebRequest.Create("http://www.google.com"));

            httpMock.Setup(c => c.DoRequest<TwitterResponse>(It.IsAny<HttpWebRequest>()))
                .Returns(new TwitterResponse() { statuses = new List<Status>() });

            return httpMock;
        }
    }
}
