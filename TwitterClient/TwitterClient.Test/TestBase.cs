﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterClient.Business;

namespace TwitterClient.Test
{
    public class TestBase
    {
        protected IContainer _container { get; set; }

        public TestBase()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<HttpService>().As<IHttpService>();
            builder.RegisterType<TwitterService>().As<ITwitterService>();
            
            this._container = builder.Build();
            
        }
    }
}
