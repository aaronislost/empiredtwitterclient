﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterClient.Business;

namespace TwitterClient.Test
{
    [TestClass]
    public class HttpServiceTests
    {

        [TestInitialize]
        public void Init()
        {

        }

        [TestMethod]
        public void HttpService_should_accept_gzip()
        {
            HttpService service = new HttpService();
            var request = service.CreateRequest("http://www.google.com");

            request.Headers["Accept-Encoding"].ShouldContain("gzip");
        }
    }
}
