﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterClient.Common;

namespace TwitterClient.Test
{
    [TestClass]
    public class CommonTests
    {
        [TestMethod]
        public void SearchResponse_should_handle_null_user()
        {
            Status s = new Status();
            SearchResponse response = new SearchResponse(s);
            response.User.ShouldBe("Unknown");
        }

        [TestMethod]
        public void SearchResponse_should_handle_a_user_with_a_name()
        {
            Status s = new Status()
            {
                user = new User()
                {
                    name = "bob"
                }
            };
            SearchResponse response = new SearchResponse(s);
            response.User.ShouldBe("@bob");
        }

        [TestMethod]
        public void SearchResponse_should_load_text()
        {
            Status s = new Status()
            {
                text = "hello"
            };

            SearchResponse response = new SearchResponse(s);

            response.Text.ShouldBe("hello");
        }


    }
}
