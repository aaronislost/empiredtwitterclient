﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterClient.Business;

namespace TwitterClient.Test
{


    [TestClass]
    public class AssemblyInit
    {
        public static IContainer Container { get; set; }

        [AssemblyInitialize]
        public static void Init(TestContext ctx)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<HttpService>().As<IHttpService>();
            builder.RegisterType<TwitterService>().As<ITwitterService>();

            Container = builder.Build();
        }
    }
}
